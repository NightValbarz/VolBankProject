package testDomain;



import org.junit.Before;
import org.junit.jupiter.api.Test;
import Domain.*;

class test3UnitCases {
	MatcherApp matcher = new MatcherApp();

	@Before
	public void setUp() throws Exception {
		matcher.addVolunteer("Juan", "Teaching", "Decorator", 2, "Night", "12345");
	}
	
	
	@Test
	void testRecipientRegister() {
		matcher.addRecipient("Laura", "Cooking", "Decorator", 2, "Laura", "laura5");
	}
	
	@Test
	void testVolunteerRegister() {
		matcher.addVolunteer("Manuel", "Teaching", "Decorator", 2, "Aslak", "aslak");
	}
	
	//Login Test
	@Test
	void test() {
		matcher.loginVolunteer("Night", "12345");
		matcher.loginVolunteer("Aslak", "aslak");
		matcher.loginRecipient("Laura", "laura5");
	}

}
