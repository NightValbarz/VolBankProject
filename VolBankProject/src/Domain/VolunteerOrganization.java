package Domain;

import java.util.List;

/**
 * @author Windows
 * @version 1.0
 * @created 16-Dec-2017 10:12:53 AM
 */
public class VolunteerOrganization extends User {

	private String name;
	private List<Volunteer> volunteers;
	private String User;
	private String password;

	public VolunteerOrganization(String name, String user, String password) {
		this.name = name;
		this.User = user;
		this.password = password;
	}

	public void addVolunteer(Volunteer o) {
		this.volunteers.add(o);
	}

	public void finalize() throws Throwable {
		super.finalize();
	}

	public String getName() {
		return name;
	}

	public List<Volunteer> getVolunteers() {
		return volunteers;
	}

	public Boolean isPassword(String password) {
		if (this.password.compareTo(password) == 0)
			return true;
		return false;
	}

	public Boolean isUser(String user) {
		if (this.User.compareTo(user) == 0)
			return true;
		return false;
	}

}