package Domain;

/**
 * @author Windows
 * @version 1.0
 * @created 16-Dec-2017 10:12:28 AM
 */
public class Time {

	private Integer quantity;

	public Time(Integer quantity) {
		this.quantity = quantity;
	}

	public void finalize() throws Throwable {

	}

	public Integer getTimeQuantity() {
		return quantity;
	}

}