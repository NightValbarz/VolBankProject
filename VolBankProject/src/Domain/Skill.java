package Domain;

/**
 * @author Windows
 * @version 1.0
 * @created 16-Dec-2017 10:12:18 AM
 */
public class Skill {

	private String name;

	public Skill(String name) {
		this.name = name;
	}

	public void finalize() throws Throwable {

	}

	public String getSkillName() {
		return name;
	}

}