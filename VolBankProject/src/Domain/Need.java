package Domain;

/**
 * @author Windows
 * @version 1.0
 * @created 16-Dec-2017 10:11:58 AM
 */
public class Need {

	private String name;

	public Need(String name) {
		this.name = name;
	}

	public void finalize() throws Throwable {

	}

	public String getNeedName() {
		return name;
	}

}