package Domain;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Windows
 * @version 1.0
 * @created 16-Dec-2017 10:11:44 AM
 */
public class MatcherApp {
	private List<User> users = new ArrayList<>();
	private List<Match> matches = new ArrayList<>();
	private List<Recipient> recipients = new ArrayList<>();
	private VolunteerOrganization volunteerOrganizations;
	private List<Volunteer> volunteers = new ArrayList<>();
	public Volunteer m_Volunteer;
	public Recipient m_Recipient;
	public VolunteerOrganization m_VolunteerOrganization;
	public Match m_Match;

	public void addRecipient(String name, String skill, String need, Integer quantity, String user, String password) {
		this.recipients.add(new Recipient(name, skill, need, quantity, user, password));
		this.users.add(new Recipient(name, skill, need, quantity, user, password));
	}

	public void addVolunteer(String name, String skill, String need, Integer quantity, String user, String password) {
		this.volunteers.add(new Volunteer(name, skill, need, quantity, user, password));
		this.users.add(new Recipient(name, skill, need, quantity, user, password));
	}

	public void addVolunterOrganization() {

	}

	public Boolean loginRecipient(String user, String password) {
		for (Recipient o : recipients) {
			if (o.isPassword(password) && o.isUser(user))
				return true;
		}
		return false;
	}

	public Boolean loginVolunteer(String user, String password) {
		for (Volunteer o : volunteers) {
			if (o.isPassword(password) && o.isUser(user))
				return true;
		}
		return false;
	}

	public void makeMatches() {

	}

	public void register() {

	}

	public Recipient registerNewRecipient() {
		return null;
	}

	public Volunteer registerNewVolunteer() {
		return null;
	}

	public VolunteerOrganization registerNewVolunteerOrganization() {
		return null;
	}

	public void updateWebServer() {

	}

}