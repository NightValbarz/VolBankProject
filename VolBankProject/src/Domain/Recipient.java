package Domain;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Windows
 * @version 1.0
 * @created 16-Dec-2017 10:12:07 AM
 */
public class Recipient extends User {

	private String name;
	private String user;
	private String password;
	private List<Skill> skill = new ArrayList<Skill>();
	private Time time;
	private Need m_Need;

	public Recipient(String name, String skill, String need, Integer time, String user, String password) {
		this.name = name;
		this.skill.add(new Skill(skill));
		this.m_Need = new Need(need);
		this.time = new Time(time);
		this.user = user;
		this.password = password;
	}

	public void addNeed(String need) {
		this.m_Need = new Need(need);
	}

	public void addSkill(String skill) {
		this.skill.add(new Skill(skill));
	}

	public String getNeed() {
		return m_Need.getNeedName();
	}

	public Integer getTime() {
		return time.getTimeQuantity();
	}

	public List<Skill> getSkills() {
		return skill;
	}

	public Boolean isPassword(String password) {
		if (this.password.compareTo(password) == 0)
			return true;
		return false;
	}

	public String getName() {
		return this.name;
	}

	public Boolean isUser(String user) {
		if (this.user.compareTo(user) == 0)
			return true;
		return false;
	}

}